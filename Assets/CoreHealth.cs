﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreHealth : MonoBehaviour {

	public float health = 750f;
	public float hitDamage = 3f;
	public GameObject flashEffect;
	public GameObject flashEffectFinal;
	public Slider healthBar;
	private CapsuleCollider myCol;	

	void Start () {
		healthBar = GameObject.Find ("CoreInfo")
			.GetComponentInChildren<Slider> ();
		healthBar.maxValue = health;
		healthBar.value = health;

		myCol = GetComponent<CapsuleCollider> ();
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "Enemy") {
			
			Hurt (hitDamage);

			DestroyEnemy (col.gameObject);
		}
	}

	void DestroyEnemy (GameObject en){
		en.GetComponent<EnemyHealth> ().Kill();
		Destroy (gameObject.GetComponent<BoxCollider> ());
	}

	void Hurt (float amount) {
		health -= amount;

		if (health > 0)
			RegularFlash ();
		else
			GameOverFlash ();
		
		healthBar.value = health;

	}

	void RegularFlash ()
	{
		GameObject flash = Instantiate (flashEffect
			, gameObject.transform.position + new Vector3 (0, 3.75f, 0)
			, Quaternion.Euler (Vector3.zero)) as GameObject;
		Destroy (flash, 1.0f);
	}

	void GameOverFlash(){
		GameObject flash = Instantiate (flashEffectFinal
			, gameObject.transform.position + new Vector3 (0, 3.75f, 0)
			,Quaternion.Euler(Vector3.zero)
		)as GameObject;
		Destroy (flash, 5.0f);
	}
}
