﻿using System.IO;
using UnityEngine;

namespace Util
{
    public class ObjectSpawner
    {
        private readonly string _pathPrefix;

        public ObjectSpawner(string pathPrefix)
        {
            _pathPrefix = pathPrefix;
        }
        
        public T SpawnObjectOfType<T>(string name, Transform parent) where T : Object
        {
            return Object.Instantiate(Resources.Load<T>(GetPath(name)), parent);
        }

        public T SpawnObjectOfType<T>(string name, Transform parent, Vector3 at) where T : Object
        {
            return Object.Instantiate(Resources.Load<T>(GetPath(name)), at, Quaternion.identity, parent);
        }

        private string GetPath(string resourceName)
        {
            return Path.Combine(_pathPrefix, resourceName);
        }
    }
}