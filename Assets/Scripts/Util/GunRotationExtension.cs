﻿using System;
using Inventory;
using UnityEngine;

namespace Util
{
    public static class GunRotationExtension
    {
        public static Quaternion ToQuaternion(this GunRotation gunRotation)
        {
            switch (gunRotation)
            {
                case GunRotation.North:
                    return Quaternion.Euler(0, 0, 0);
                case GunRotation.South:
                    return Quaternion.Euler(0, 180, 0);
                case GunRotation.West:
                    return Quaternion.Euler(0, 270, 0);
                case GunRotation.East:
                    return Quaternion.Euler(0, 90, 0);
                default:
                    throw new ArgumentOutOfRangeException("gunRotation", gunRotation, null);
            }
        }
    }
}