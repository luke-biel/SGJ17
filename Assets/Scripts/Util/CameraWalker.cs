﻿using UnityEngine;

namespace Util
{
    public class CameraWalker : MonoBehaviour
    {
        public Vector3 Direction;
        public Transform Camera;
        public float MoveSpeed = 5.0f;
        
        private bool _isMouseOver = false;

        private void Update()
        {
            if (_isMouseOver)
            {
                Camera.position += Time.deltaTime * MoveSpeed * Direction;
            }
        }
        
        public void MouseEnter()
        {
            _isMouseOver = true;
        }

        public void MouseExit()
        {
            _isMouseOver = false;
        }
    }
}