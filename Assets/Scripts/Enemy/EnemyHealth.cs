﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public float health = 20f;
	private bool dead = false;

	private Animator anim;

	public void Start(){
		anim = GetComponent<Animator>();
	}

	public void Hurt (float amount){
		health -= amount;
		Die ();
	}

	public void Kill (){
		health = 0;
		Die ();
	}

	protected void Die(){
		if (health <= 0 && !dead) {
			//Stop moving here!!
			gameObject.tag = "Untagged";
			dead = true;
			anim.SetBool ("Die", true);
			GameObject.Destroy (gameObject, 1.1f);
		}
	}
}
