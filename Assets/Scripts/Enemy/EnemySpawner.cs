﻿using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;
using UnityEngine.AI;
using Util;

public class EnemySpawner : MonoBehaviour
{
	public static Transform Core;
	public bool isPaused = true;
	public GameObject[] enemies;
	public float leftInWave;
	public Vector3 currentSpawnPoint;
	private const float spawnPointOffset = 3f;
	private Main mainScript;
	private GameObject[] availableTiles;

	private float _timePassed;

	void Start () 
	{
		mainScript = GameObject.FindObjectOfType<Camera> ()
			.GetComponent<Main>();

		availableTiles = FindAvailableTiles ();
		StartCoroutine ("SpawnEnemies");

	}

	void FixedUpdate ()
	{
		_timePassed += Time.fixedDeltaTime;
		if (leftInWave < 1 && !isPaused) {

			leftInWave = Random.Range (3, 4 + (_timePassed / 10));

			currentSpawnPoint = FindNewSpawnPoint ();
			currentSpawnPoint.y += spawnPointOffset;

		} else if (isPaused){
			leftInWave = 0;
		}
	}

	IEnumerator SpawnEnemies ()
	{
		while (true)
		{
			if (!isPaused && leftInWave >= 1)
			{
				NavMeshHit closestHit;
				if (!NavMesh.SamplePosition(currentSpawnPoint, out closestHit, 500f, NavMesh.AllAreas))
					Debug.Log("Fail");
				GameObject enemy = Instantiate (enemies [Random.Range (0, enemies.Length)],
					                   closestHit.position, 
					                   Quaternion.Euler (Vector3.zero));
				enemy.AddComponent<Enemy>();
				enemy.tag = "Enemy";
				
				leftInWave--;
			}
			yield return new WaitForSeconds (0.5f);
		}
	}

	Vector3 FindNewSpawnPoint(){
		Vector3 result = Vector3.zero;

		int rand = Random.Range (0, availableTiles.Length);

		result = availableTiles [rand].transform.position;

		return result;
	}

	GameObject[] FindAvailableTiles (){
		List<GameObject> result = new List<GameObject>();
		int min = ((Gameplay.Map.MapSize - 1) / 2) * -1;
		int max = min * -1;

		foreach (Gameplay.Tile tile in mainScript.Map.Tiles) {
			if ( ( tile.transform.position.x == min ) ||
			     ( tile.transform.position.z == min ) ||
			     ( tile.transform.position.x == max ) ||
			     ( tile.transform.position.z == max ) ) 
					{
						result.Add (tile.gameObject);
					}
		}

		return result.ToArray();
	}
}
