﻿using Gameplay;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace Inventory
{
    public class GunSelector : MonoBehaviour
    {
        public GunRotation GunRotation = GunRotation.North;
        
        private Map _map;
        private ObjectSpawner _objectSpawner;
        private string _selectedGun = null;
        private Transform _transform;

        private static readonly string[] Guns = new[] { "RotorGun" };

        private void Awake()
        {
            _transform = transform;
        }

        public void Setup(ObjectSpawner objectSpawner, Map map)
        {
            _objectSpawner = objectSpawner;
            _map = map;
        }

        public void Initialize()
        {
            InitializeGUI();
            _map.OnNewTileHighlighted += SpawnShadow;
        }

        public void SelectGun(string gunName)
        {
            _selectedGun = gunName;
        }
		public bool isPlacingGun()
		{
			return (_selectedGun != null);
		}

        private void InitializeGUI()
        {
            foreach (var gun in Guns)
            {
                var gunComponent = _objectSpawner.SpawnObjectOfType<Gun>(gun + "Icon", _transform);
                var button = gunComponent.GetComponent<Button>();
                button.onClick.AddListener(() => SelectGun(gun));
                gunComponent.Setup(this);
            }
        }

        private void SpawnShadow(Tile tile)
        {
            if (_selectedGun == null) return;
            tile.SpawnAbove(_selectedGun);
        }
    }
}