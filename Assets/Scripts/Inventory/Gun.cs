﻿using UnityEngine;

namespace Inventory
{
    public class Gun : MonoBehaviour
    {
        private GunSelector _gunSelector;
        
        public void Setup(GunSelector gunSelector)
        {
            _gunSelector = gunSelector;
        }

        public void SelectGun(string gunName)
        {
            _gunSelector.SelectGun(gunName);
        }
    }
}