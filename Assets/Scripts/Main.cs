﻿using Gameplay;
using Inventory;
using Player;
using UnityEngine;
using Util;

public class Main : MonoBehaviour
{
    public Map Map;
    public TileSelector TileSelector;
    public GunSelector GunSelector;
    
    private ObjectSpawner _objectSpawner;

    private void Awake()
    {
        _objectSpawner = new ObjectSpawner("ObjectSpawner");
        
        Map.Setup(_objectSpawner, TileSelector);
        GunSelector.Setup(_objectSpawner, Map);
    }

    private void Start()
    {
        Map.Initialize();
        GunSelector.Initialize();
    }
}
