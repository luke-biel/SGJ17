﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelling : MonoBehaviour {

	public int SellCost = 2;
	private Player.TileSelector TSScript;
	private Camera cam;
	private PlayerPurchasing PPScript;

	void Start () {
		cam = FindObjectOfType<Camera> ();
		PPScript = GetComponent<PlayerPurchasing> ();
	}

	void Update () {
		Ray ray = cam.ScreenPointToRay (Input.mousePosition);
		Debug.DrawRay (ray.origin, ray.direction * 100);
		RaycastHit raycastHit;

		if (Physics.Raycast (ray, out raycastHit, 100.0f)) {
			Collider tower = raycastHit.collider.GetComponent<BoxCollider> ();
			if (tower == null)
				return;
			if (Input.GetButton("Fire2")){
				Gameplay.Tile tileBelow = tower.transform.
					parent.parent.
					GetComponent<Gameplay.Tile> ();

				PPScript.AddScore (SellCost);
	
				DestroyImmediate(tower);
				tileBelow.CanBuildOn = true;
				tileBelow.SpawnAbove(null);
			}
		}
	}
}
