﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPurchasing : MonoBehaviour {

	public int score = 30;
	public int scoreIncome = 2;
	public int scoreMax = 60;

	public Slider resourcesBar;

	void Start () {
		resourcesBar.maxValue = scoreMax;
		StartCoroutine ("RegularIncome");
	}

	IEnumerator RegularIncome(){
		while (true) {
			AddScore (scoreIncome);			
			yield return new WaitForSeconds (1.0f);
		}
	}

	void UpdateScore (){
		resourcesBar.value = score;
	}
	 
	public void AddScore (int amount){
		if (score + amount < scoreMax)
			score += amount;
		else
			score = scoreMax;

		UpdateScore ();
	}

	public void Pay (int amount){
		AddScore (amount * -1);
	}
}
