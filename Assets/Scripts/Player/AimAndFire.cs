﻿using System.Collections;
using UnityEngine;

public class AimAndFire : MonoBehaviour {

    public Transform closests;
    public Transform myTransform;
    private AudioSource audioSource;
    public float[] lineRendereOffsets = {0.85f, 0.5f};
    public LineRenderer line;
    private Animator anim;

    void Start(){
        anim = GetComponentInParent<Animator> ();
        line = GetComponent<LineRenderer> ();
        myTransform = gameObject.transform;
        audioSource = GetComponent<AudioSource>(); 
        StartCoroutine (Seek());
        StartCoroutine (Fire());
    }

    void FixedUpdate () {
        gameObject.transform.LookAt (closests);
    }

    protected virtual IEnumerator Fire () {
        while (true) {
            if (closests != null) {
                if(IsInLineOfSight(closests.transform.position)){
                    EnemyHealth EH = closests.gameObject.GetComponent<EnemyHealth> ();
                    EH.Hurt (100f);

                    line.enabled = true;
                    DrawLine (line);
                    anim.SetBool ("Fire", true);
                    
                    audioSource.Play();

                    yield return new WaitForSeconds (0.33f);
                    anim.SetBool ("Fire", false);
                    line.enabled = false;
                }
            }
            yield return new WaitForSeconds (1.0f);
        }
    }

    protected virtual void DrawLine (LineRenderer l) {
        Vector3[] points = {myTransform.position + new Vector3(0f,lineRendereOffsets[0],0f)
            , closests.position + new Vector3(0f,lineRendereOffsets[1],0f)};
        l.SetPositions(points);
    }

    protected virtual IEnumerator Seek () {
        while (true) {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

            if (enemies.Length > 0) {
                closests = enemies [0].transform;
                float distance = Vector3.Distance (myTransform.position, enemies[0].transform.position);

                foreach (GameObject en in enemies) {
                    float newDistance = Vector3.Distance (myTransform.position, en.transform.position);

                    if (!IsInLineOfSight(en.transform.position))
                        continue;

                    if (newDistance < distance) {
                        distance = newDistance;
                        closests = en.transform;
                    }
                }
                if (distance > 5.0f) {
                    closests = null;
                }
            }
            yield return new WaitForSeconds (0.01f);
        }
    }

    bool IsInLineOfSight(Vector3 other)
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(transform.position, other - transform.position, out raycastHit))
        {
            if (raycastHit.collider.CompareTag("Enemy"))
            {
                return true;
            }
        }
        return false;
    }
}
