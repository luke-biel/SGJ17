﻿using Gameplay;
using UnityEngine;
using Util;
using Inventory;

namespace Player
{
    public delegate void OnNewTileSelected(Tile tile);
    
    public class TileSelector : MonoBehaviour
    {
        public const string TileLayerName = "Tile";
        
        public Camera Camera;

        public Tile SelectedTile { get; private set; }
        public event OnNewTileSelected OnNewTileSelected;

		private PlayerPurchasing PPScript;
		public int TowerCost = 4;

		private GunSelector GSScript;

		private Vector3 spawnOffset;

		public void Start() {
			GSScript = FindObjectOfType<Camera> ()
				.GetComponent<Main> ().GunSelector;
			PPScript = GameObject.Find("PlayerManager")
				.GetComponent<PlayerPurchasing>();

			spawnOffset = new Vector3 (0f, -0.35f, 0f);
		}
        
        public void Update()
        {
			bool isButtonOneDown = Input.GetButtonDown ("Fire1");
			if (Camera == null)
					return;
				var ray = Camera.ScreenPointToRay (Input.mousePosition);
				Debug.DrawRay (ray.origin, ray.direction * 100);
				RaycastHit raycastHit;
			if (Physics.Raycast (ray, out raycastHit, 100.0f)) 
			{
				var newTile = raycastHit.collider.GetComponent<Tile> ();
				if (newTile == null)
					return;
				if (isButtonOneDown && newTile.CanBuildOn) {					
					if (PPScript.score >= TowerCost) {
						if (GSScript.isPlacingGun ()) {
							ObjectSpawner spawner = new ObjectSpawner ("ObjectSpawner");

							SelectedTile.SpawnAbove (null);
							spawner.SpawnObjectOfType<GameObject> ("RotorGunGame", newTile.PointAbove, newTile.PointAbove.position + spawnOffset);

							newTile.CanBuildOn = false;
							PPScript.Pay (TowerCost);
						}
					}
					return;
				}
				if (SelectedTile == newTile)
					return;
				if (SelectedTile != null) 
				{
					SelectedTile.SpawnAbove (null);
				}
				SelectedTile = newTile;
				if (OnNewTileSelected != null) 
				{
					OnNewTileSelected (SelectedTile);
				}
			}
            else
            {
                if (SelectedTile == null) return;
                SelectedTile.SpawnAbove(null);
                SelectedTile = null;
                if (OnNewTileSelected != null)
                {
                    OnNewTileSelected(null);
                }
            }
        }
    }
}