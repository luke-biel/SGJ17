﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

public class PlayerTurretHealth : EntityHealth {
	
	protected override void Die (){
		GameObject.Destroy (gameObject, 1.0f);
	}

	public override void Hurt (float amount){
		base.Hurt (amount);
	}

	public void Dissamble(){
		Die ();
	}

	public override void Start(){}
}
