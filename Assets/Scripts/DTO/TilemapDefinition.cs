﻿using System;

namespace DTO
{
    [Serializable]
    public class TilemapDefinition
    {
        public Layer[] layers;
    }
}