﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityHealth : MonoBehaviour {

	protected float health = 20f;

	public virtual void Hurt (float amount){
		health -= amount;
	}

	public virtual void Start (){}
	protected virtual void Die (){}
}