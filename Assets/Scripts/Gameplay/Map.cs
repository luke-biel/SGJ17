﻿using System.Collections;
using System.Collections.Generic;
using DTO;
using Player;
using UnityEngine;
using Util;

namespace Gameplay
{
    public delegate void OnNewTileHighlighted(Tile tile);
    
    public class Map : MonoBehaviour
    {
        public const int MapSize = 31;
        private const int Offset = MapSize / 2;

        public Tile[,] Tiles;
        public event OnNewTileHighlighted OnNewTileHighlighted;
        public TilemapDefinition TilemapDefinition;

        private int _currentLayer = 0;
        private ObjectSpawner _objectSpawner;
        private Transform _transform;
        private TileSelector _tileSelector;


        private void Awake()
        {
            Tiles = new Tile[MapSize, MapSize];
            _transform = transform;
        }

        public void Setup(ObjectSpawner objectSpawner, TileSelector tileSelector)
        {
            _objectSpawner = objectSpawner;
            _tileSelector = tileSelector;

            TilemapDefinition = JsonUtility.FromJson<TilemapDefinition>(
                Resources.Load<TextAsset>("LevelDesign/Level").text);
        }

        public void Initialize()
        {
            for (var x = 0; x < MapSize; x++)
            {
                for (var y = 0; y < MapSize; y++)
                {
                    if (x == Offset && y == Offset)
                    {
                        Tiles[x, y] = _objectSpawner.SpawnObjectOfType<CoreTile>(
                            CoreTile.ResourceName,
                            _transform,
                            new Vector3(0, 0, 0));
                    }
                    else
                    {
                        Tiles[x, y] = _objectSpawner.SpawnObjectOfType<GroundTile>(
                            GroundTile.ResourceName,
                            _transform,
                            new Vector3(x - Offset, 0, y - Offset));
                    }
                    Tiles[x, y].Setup(_objectSpawner);
                }
            }

            _tileSelector.OnNewTileSelected += HighlightTile;

            StartCount();
        }

        private void StartCount()
        {
            StartCoroutine(NextMap());
        }

        private IEnumerator NextMap()
        {
            while (true)
            {
                _currentLayer = Mathf.Max(1, (_currentLayer + 1) % TilemapDefinition.layers.Length);

                yield return new WaitForSeconds(15);
                FlashTiles();
                yield return new WaitForSeconds(5);
                RaiseWalls();
            }
        }

        private void RaiseWalls()
        {
            var currentLayer = TilemapDefinition.layers[_currentLayer];
            for (var x = 0; x < MapSize; x++)
            {
                for (var y = 0; y < MapSize; y++)
                {
                    var groundTile = Tiles[x, y] as GroundTile;
                    if (groundTile != null) groundTile.UnMakeWall();
                }
            }
            
            for (var i = 0; i < MapSize * MapSize; i++)
            {
                if (currentLayer.data[i] != 2) continue;
                var point = IndexToPosition(i);
                var groundTile = Tiles[point.Value, point.Key] as GroundTile;
                if(groundTile != null) groundTile.MakeWall();
            }
        }

        private void FlashTiles()
        {
            
        }

        private void HighlightTile(Tile tile)
        {
            if (OnNewTileHighlighted != null)
            {
                OnNewTileHighlighted(tile);
            }
        }

        private KeyValuePair<int, int> IndexToPosition(int index)
        {
            return new KeyValuePair<int, int>(index % MapSize, index / MapSize);
        }

        private int PositionToIndex(KeyValuePair<int, int> position)
        {
            return position.Key * MapSize + position.Value;
        }
    }
}