﻿using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{
    public class Enemy : MonoBehaviour
    {
		private Animator anim;
        private NavMeshAgent navMeshAgent;

        private void Awake()
        { 
			anim = GetComponent<Animator> ();
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            navMeshAgent.SetDestination(Vector3.zero);
			anim.SetFloat ("Speed", navMeshAgent.velocity.magnitude);
			if (anim.GetBool ("Die"))
				navMeshAgent.speed = 0;
        }
    }
}