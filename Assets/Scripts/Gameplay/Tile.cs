﻿using Inventory;
using UnityEngine;
using Util;

namespace Gameplay
{
    public abstract class Tile : MonoBehaviour
    {
        public Transform PointAbove;
        public bool CanBuildOn = true;

        protected ObjectSpawner _objectSpawner;

        public void Setup(ObjectSpawner objectSpawner)
        {
            _objectSpawner = objectSpawner;
        }

        public virtual void SpawnAbove(string selectedGun)
        {
            if (!CanBuildOn) return;
            if (selectedGun == null)
            {
                foreach (Transform child in PointAbove.transform)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
            else
            {
                _objectSpawner.SpawnObjectOfType<GameObject>(selectedGun, PointAbove);
            }
        }
    }
}