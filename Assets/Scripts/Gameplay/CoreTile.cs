﻿namespace Gameplay
{
    public class CoreTile : Tile
    {
        public const string ResourceName = "Core";
        
        public override void SpawnAbove(string selectedGun)
        {
            // Intentionally left blank
        }
    }
}