﻿using UnityEngine;

namespace Gameplay
{
    public class GroundTile : Tile
    {
        public const string ResourceName = "Ground";

        public bool IsWall = false;

        public void MakeWall()
        {
            if (IsWall) return;
            IsWall = true;
            SpawnAbove(null);
            _objectSpawner.SpawnObjectOfType<GameObject>("WallTile", PointAbove);
        }

        public void UnMakeWall()
        {
            if (!IsWall) return;
            IsWall = false;

            foreach (Transform child in PointAbove.transform)
            {
                DestroyImmediate(child.gameObject);
            }
        }
    }
}